import 'package:cheff/models/appUserInfo.dart';
import 'package:cheff/models/packs.dart';
import 'package:cheff/models/subscriptions.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class UserSubscriptionsProvider extends ChangeNotifier {
  List<UserSubscription> userSubscriptions = [];

  int preferredCard = 0;
  int preferredLocation = 0;
  bool _isLoaded = false;
  bool _isLoading = false;
  bool _hasSubscribed = false;

  void reset() {
    _isLoaded = false;
    _isLoading = false;
    notifyListeners();
  }

  void loadData() {
    if (FirebaseAuth.instance.currentUser == null || _isLoading || _isLoaded) {
      return;
    }
    _isLoading = true;
    _isLoaded = false;

    FirebaseFirestore.instance
        .collection('userSubscriptions')
        .doc(FirebaseAuth.instance.currentUser.uid)
        .collection('active')
        .get()
        .then((snapshot) {
      userSubscriptions.clear();
      for (var subs in snapshot.docs) {
        UserSubscription subscription = new UserSubscription();

        subscription.id = subs.id;
        subscription.packId = subs.get('packId').toString();
        subscription.currentStatus = subs.get('currentStatus').toString();
        subscription.nextDeliveryDate = subs.get('nextDeliveryDate').toString();
        subscription.restaurantName = subs.get('restaurantName').toString();
        subscription.imageUrl = subs.get('imageUrl').toString();
        subscription.cardId = subs.get('cardId').toString();
        subscription.locationId = subs.get('locationId').toString();

        userSubscriptions.add(subscription);
      }
      _isLoading = false;
      _isLoaded = true;

      notifyListeners();
      if (_hasSubscribed) {
        return;
      }
      _hasSubscribed = true;
      FirebaseAuth.instance.authStateChanges().listen((User user) {
        reset();
      });
    }).catchError((error) {
      print(error);

      _isLoading = false;
      _isLoaded = false;
    });
  }

  bool hasSubscribedToPack(String packId) {
    bool hasSubscribed = false;
    for (var sub in userSubscriptions) {
      hasSubscribed = hasSubscribed || sub.packId == packId;
    }
    return hasSubscribed;
  }

  void addSubscription(RestaurantPackRow pack, UserLocation location, UserCard card, VoidCallback callback) {
    FirebaseFirestore.instance
        .collection('userSubscriptions')
        .doc(FirebaseAuth.instance.currentUser.uid)
        .collection('active')
        .add({
      'packId': pack.packId,
      'currentStatus': 'NONE',
      'nextDeliveryDate': pack.deliveryTime,
      'restaurantName': pack.restaurantName,
      'imageUrl': pack.imageUrl,
      'cardId': card.cardId,
      'locationId': location.id
    }).then((value) {
      UserSubscription subscription = new UserSubscription();
      subscription.id = value.id;
      subscription.packId = pack.packId;
      subscription.currentStatus = 'NONE';
      subscription.nextDeliveryDate = pack.deliveryTime;
      subscription.restaurantName = pack.restaurantName;
      subscription.imageUrl = pack.imageUrl;
      subscription.cardId = card.cardId;
      subscription.locationId = location.id;

      userSubscriptions.add(subscription);
      notifyListeners();
      callback();
    }).catchError((error) {
      print(error);
      callback();
    });
  }
}
