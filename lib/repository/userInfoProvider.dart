import 'package:cheff/models/appUserInfo.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserInfoProvider extends ChangeNotifier {
  AppUserInfo userInfo;

  BaseUserController _userController = FirebaseUserController();

  // VoidCallback<String> onError;

  bool _isLoaded = false;
  bool _isLoading = false;
  bool _hasSubscribed = false;

  void reset() {
    _isLoaded = false;
    _isLoading = false;
    notifyListeners();
  }

  void loadData() {
    var user = FirebaseAuth.instance.currentUser;

    if (user == null || _isLoading || _isLoaded) {
      return;
    }

    userInfo = new AppUserInfo();
    _isLoaded = false;
    _isLoading = true;

    _userController.getUser(user.uid).then((userInfo) async {
      this.userInfo = userInfo;
    }).catchError((error) {
      _isLoading = false;

      print(error);
    });
  }

  void addLocation(UserLocation userLocation) {
    FirebaseFirestore.instance
        .collection('users')
        .doc(FirebaseAuth.instance.currentUser.uid)
        .collection('locations')
        .add({
      'lat': userLocation.lat,
      'long': userLocation.long,
      'streetName': userLocation.streetName,
      'floor': userLocation.floor,
      'flatNumber': userLocation.flatNumber,
      'additionalInfo': userLocation.additionalInfo
    }).then((value) {
      userLocation.id = value.id;
      userInfo.locations.add(userLocation);
      print('added location');
      notifyListeners();
    }).catchError((error) {
      print(error);
    });
  }

  UserLocation getPreferredLocation() {
    return userInfo.locations[0];
  }

  UserCard getPreferredCard() {
    return userInfo.cards[0];
  }

  void setPreferredPaymentMethod(UserCard card) {}

  void setPreferredLocation(UserLocation location) {}

  void addCard(UserCard card) {
    FirebaseFirestore.instance.collection('users').doc(FirebaseAuth.instance.currentUser.uid).collection('cards').add(
        {'cardNumber': card.cardNumber, 'cardName': card.cardName, 'CVV': card.CVV, 'date': card.date}).then((value) {
      card.cardId = value.id;
      userInfo.cards.add(card);
      print('card was added');
      notifyListeners();
    }).catchError((error) {
      print(error);
    });
  }
}

abstract class BaseUserAuthenticationProvider {
  String getId();
}

class FirebaseAuthenticationProvider extends BaseUserAuthenticationProvider {
  @override
  String getId() {
    return FirebaseAuth.instance.currentUser.uid;
  }
}

class FirebaseUserController extends BaseUserController {
  @override
  void addCard(UserCard card) {
    // TODO: implement addCard
  }

  @override
  void addLocation(UserLocation location) {
    // TODO: implement addLocation
  }

  @override
  Future<AppUserInfo> getUser(String userId) async {
    // TODO: implement getUser
    var snapshot = await FirebaseFirestore.instance.collection('users').doc(userId).get();

    var data = snapshot.data();
    AppUserInfo userInfo = AppUserInfo();

    if (data.containsKey('name')) {
      userInfo.name = data['name'];
    }
    var cardCollectionSnapshot =
        await FirebaseFirestore.instance.collection('users').doc(userId).collection('cards').get();

    var locationCollectionSnapshot =
        await FirebaseFirestore.instance.collection('users').doc(userId).collection('locations').get();
    // if (data.containsKey('cards')) {}
    // if (data.containsKey('locations')) {}

    List<UserCard> cards = [];

    for (var card in cardCollectionSnapshot.docs) {
      var data = card.data();
      UserCard localCard = UserCard(data['cardNumber'], data['date'], data['CVV'], data['cardName']);
      localCard.cardId = card.id;
      cards.add(localCard);
    }

    userInfo.cards = cards;

    List<UserLocation> locations = [];

    for (var location in locationCollectionSnapshot.docs) {
      UserLocation localLocation = new UserLocation();
      localLocation.id = location.id;
      localLocation.lat = double.parse(location.get('lat').toString());
      localLocation.long = double.parse(location.get('long').toString());
      localLocation.streetName = location.get('streetName').toString();
      localLocation.floor = int.parse(location.get('floor').toString());
      localLocation.flatNumber = location.get('flatNumber').toString();
      localLocation.additionalInfo = location.get('additionalInfo').toString();
      locations.add(localLocation);
    }

    userInfo.locations = locations;

    return Future<AppUserInfo>.value(userInfo);
  }
}

abstract class BaseUserController {
  Future<AppUserInfo> getUser(String userId);

  void addCard(UserCard card);

  void addLocation(UserLocation location);
}
