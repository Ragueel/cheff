import 'package:cheff/models/packs.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class PacksProvider extends ChangeNotifier {
  List<RestaurantPackRow> packs = [];

  bool _isLoaded = false;
  bool _isLoading = false;

  bool get isLoading => _isLoading;

  void loadData() {
    if (_isLoaded || _isLoading) {
      return;
    }

    _isLoaded = false;
    _isLoading = true;
    FirebaseFirestore.instance.collection('packs').get().then((snapshot) {
      packs.clear();
      for (QueryDocumentSnapshot querySnapshot in snapshot.docs) {
        var data = querySnapshot.data();
        RestaurantPackRow restaurantPackRow =
            new RestaurantPackRow(querySnapshot.id, data['restaurantName'].toString());
        restaurantPackRow.imageUrl = data['imageUrl'].toString();
        restaurantPackRow.price = double.parse(data['price'].toString());
        restaurantPackRow.rating = double.parse(data['rating'].toString());
        restaurantPackRow.deliveryTime = data['deliveryTime'].toString();
        restaurantPackRow.days = [];
        for (var dayInfoSnapshot in data['dayInfo']) {
          DayInfo dayInfo = new DayInfo();
          dayInfo.imageUrl = dayInfoSnapshot['imageUrl'];
          dayInfo.dayName = dayInfoSnapshot['dayName'];
          dayInfo.foodName = dayInfoSnapshot['foodName'];
          dayInfo.foodDescription = dayInfoSnapshot['foodDescription'];
          restaurantPackRow.days.add(dayInfo);
        }
        restaurantPackRow.categories = [];

        for (var category in data['categories']) {
          restaurantPackRow.categories.add(category.toString());
        }

        packs.add(restaurantPackRow);
      }
      _isLoaded = false;
      _isLoading = false;

      notifyListeners();
    }).catchError((error) {
      _isLoaded = false;
      _isLoading = false;
      print(error);
    });
  }
}

