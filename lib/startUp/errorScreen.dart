import 'package:flutter/material.dart';

class StartUpError extends StatelessWidget {
  final style = TextStyle(color: Colors.red, fontSize: 19.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Text(
        'Something went wrong',
        style: style,
      ),
    );
  }
}
