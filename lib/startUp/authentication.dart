import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AuthenticationPage extends StatefulWidget {
  @override
  _AuthenticationPageState createState() => _AuthenticationPageState();
}

class _AuthenticationPageState extends State<AuthenticationPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;
  String _loginError;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: buildContainer(),
      ),
    );
  }

  Container buildContainer() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: _isLoading ? CircularProgressIndicator() : buildLoginForm(),
    );
  }

  Column buildLoginForm() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center, //Center Column contents vertically,
      // crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          'Log In',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
        ),
        buildFormFields(),
        SizedBox(
          height: 20,
        ),
        _loginError == null
            ? SizedBox()
            : Text(
                _loginError,
                style: TextStyle(color: Colors.red),
              ),
        Row(
          children: [
            Expanded(
                child: RawMaterialButton(
              onPressed: _signIn,
              elevation: 0,
              child: Text(
                'Sign in',
                style: TextStyle(color: Colors.white),
              ),
              fillColor: Colors.redAccent,
            ))
          ],
        ),
        Row(
          children: [
            Expanded(
                child: TextButton(
              onPressed: _signUp,
              child: Text('Don\'t have account?'),
            ))
          ],
        )
      ],
    );
  }

  Form buildFormFields() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
              controller: emailController,
              validator: AuthenticationUtils.validateEmail,
              decoration: InputDecoration(labelText: 'Email', hintText: 'email@test.com')),
          TextFormField(
            obscureText: true,
            controller: passwordController,
            decoration: InputDecoration(labelText: 'Password'),
            maxLength: 30,
            validator: AuthenticationUtils.validatePassword,
          )
        ],
      ),
    );
  }

  void _signIn() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    setState(() {
      _isLoading = true;
    });
    FirebaseAuth.instance
        .signInWithEmailAndPassword(email: emailController.text, password: passwordController.text)
        .then((value) {
      setState(() {
        _isLoading = false;
      });
    }).catchError((error) {
      if (error.code == 'user-not-found') {
        _loginError = 'No user found for that email.';
      } else if (error.code == 'wrong-password') {
        _loginError = 'Wrong password provided for that user.';
      }

      setState(() {
        _isLoading = false;
      });
    });
  }

  void _signUp() {
    Navigator.push(context, MaterialPageRoute(builder: (cont) => RegisterPage()));
  }
}

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: Center(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: _isLoading ? CircularProgressIndicator() : buildRegistrationForm(),
        ),
      ),
    );
  }

  Column buildRegistrationForm() {
    return Column(
      // mainAxisAlignment: MainAxisAlignment.center, //Center Column contents vertically,
      children: [
        Text(
          'Register',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
        ),
        buildForm(),
        SizedBox(
          height: 20,
        ),
        Row(
          children: [
            Expanded(
                child: RawMaterialButton(
              onPressed: _onRegisterClick,
              fillColor: Theme.of(context).accentColor,
              child: Text(
                'Register',
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ))
          ],
        )
      ],
    );
  }

  Form buildForm() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            controller: emailController,
            decoration: InputDecoration(labelText: 'Email', hintText: 'test@test.com'),
            validator: AuthenticationUtils.validateEmail,
          ),
          TextFormField(
            controller: nameController,
            decoration: InputDecoration(labelText: 'Your name', hintText: 'Your name'),
            validator: (value) {
              if (value.isEmpty) {
                return 'Enter your name';
              }
              return null;
            },
          ),
          TextFormField(
            obscureText: true,
            controller: passwordController,
            decoration: InputDecoration(labelText: 'Password'),
            validator: AuthenticationUtils.validatePassword,
          ),
          TextFormField(
            obscureText: true,
            controller: confirmPasswordController,
            decoration: InputDecoration(labelText: 'Confirm Password'),
            validator: (value) {
              var error = AuthenticationUtils.validatePassword(value);
              if (error != null) {
                return error;
              }
              if (value != passwordController.value.text) {
                return 'Passwords do not match';
              }
              return null;
            },
          ),
        ],
      ),
    );
  }

  void _onRegisterClick() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    setState(() {
      _isLoading = true;
    });
    try {
      FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: emailController.text, password: passwordController.text)
          .then((value) {
        FirebaseFirestore.instance
            .collection('users')
            .doc(value.user.uid)
            .set({'name': nameController.text, 'createdAt': DateTime.now().millisecondsSinceEpoch}).then((val) {
          setState(() {
            _isLoading = false;
          });
          Navigator.pop(context);
        });
      }).catchError((error) {
        print(error);
        setState(() {
          _isLoading = false;
        });
        showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  content: Text(error.toString()),
                ));
      });
    } on FirebaseAuthException catch (e) {
      print(e.message);
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                content: Text(e.message),
              ));
    }
  }
}

class AuthenticationUtils {
  static String validateEmail(String email) {
    if (email.isEmpty) {
      return 'Enter email';
    }
    if (!AuthenticationUtils.isValidEmail(email)) {
      return 'Email is not valid';
    }
    return null;
  }

  static bool isValidEmail(String em) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(em);
  }

  static String validatePassword(text) {
    if (text.isEmpty) {
      return 'Enter password';
    }
    if (text.length < 6) {
      return 'At least 6 characters';
    }
    return null;
  }
}
