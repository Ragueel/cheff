import 'package:cheff/repository/userInfoProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PersonalInfoPage extends StatefulWidget {
  @override
  _PersonalInfoPageState createState() => _PersonalInfoPageState();
}

class _PersonalInfoPageState extends State<PersonalInfoPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Personal Info'),
          elevation: 0,
          backgroundColor: Colors.white,
        ),
        body: Consumer<UserInfoProvider>(builder: (context, provider, child) {
          nameController.text = provider.userInfo.name;
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  TextFormField(
                    controller: nameController,
                    decoration: InputDecoration(labelText: 'Name', hintText: 'Name'),
                    validator: (val) {
                      if (val.isEmpty) {
                        return 'Enter something';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: RawMaterialButton(
                        fillColor: Theme.of(context).accentColor,
                        onPressed: () {
                          if (!_formKey.currentState.validate()) {
                            return;
                          }
                          provider.userInfo.name = nameController.text;
                          Navigator.pop(context);
                        },
                        child: Text(
                          'Save',
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        ),
                      ))
                    ],
                  )
                ],
              ),
            ),
          );
        }));
  }
}
