import 'package:cheff/models/appUserInfo.dart';
import 'package:cheff/pages/addPaymentMethodPage.dart';
import 'package:cheff/repository/userInfoProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PaymentInfoPage extends StatefulWidget {
  @override
  _PaymentInfoPageState createState() => _PaymentInfoPageState();
}

class _PaymentInfoPageState extends State<PaymentInfoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Payment info'),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: Container(
        child: Consumer<UserInfoProvider>(
          builder: (context, provider, widget) {
            return ListView.builder(
                itemCount: provider.userInfo.cards.length,
                itemBuilder: (context, index) {
                  return PaymentCardButton(
                    provider.userInfo.cards[index],
                    isPrefered: index == 0,
                  );
                });
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (cont) => AddPaymentMethodPage()));
        },
      ),
    );
  }
}

class PaymentCardButton extends StatelessWidget {
  final UserCard card;
  bool isPrefered = false;

  PaymentCardButton(this.card, {Key key, this.isPrefered = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        var userInfoProvider = Provider.of<UserInfoProvider>(context, listen: false);
        userInfoProvider.setPreferredPaymentMethod(card);
      },
      onLongPress: () {},
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Row(
          children: [
            this.isPrefered ? Icon(Icons.star) : SizedBox(),
            Icon(Icons.payment_outlined),
            SizedBox(
              width: 15,
            ),
            Expanded(
                child: Text(
              "Card ending with: ****${card.cardNumber.substring(card.cardNumber.length - 4, card.cardNumber.length)}",
              style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
            )),
            Icon(Icons.chevron_right)
          ],
        ),
      ),
    );
  }
}
