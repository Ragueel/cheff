import 'package:cheff/pages/chooseLocationPage.dart';
import 'package:cheff/repository/userInfoProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LocationsPage extends StatefulWidget {
  @override
  _LocationsPageState createState() => _LocationsPageState();
}

class _LocationsPageState extends State<LocationsPage> {
  _showMaterialDialog() {
    showDialog(
        context: context,
        builder: (_) => new AlertDialog(
              title: new Text("Material Dialog"),
              content: new Text("Hey! I'm Coflutter!"),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close me!'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Locations'),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Colors.white,
      body: Container(
        // padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
        child: Consumer<UserInfoProvider>(
          builder: (context, provider, widget) {
            return ListView.builder(
                itemCount: provider.userInfo.locations.length,
                itemBuilder: (context, index) {
                  var location = provider.userInfo.locations[index];
                  return LocationButton(location.streetName, "Floor ${location.floor}, Flat: ${location.flatNumber}");
                });
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (cont) => ChooseLocationPage()));
        },
      ),
    );
  }
}

class LocationButton extends StatelessWidget {
  final String title;
  final String subtitle;

  LocationButton(this.title, this.subtitle, {key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        var userInfoProvider = Provider.of<UserInfoProvider>(context, listen: false);
        // userInfoProvider.setPreferredLocation(loca);
      },
      onLongPress: () {},
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: Row(
          children: [
            Icon(Icons.edit_location_outlined),
            SizedBox(
              width: 15,
            ),
            Expanded(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
                ),
                Text(subtitle)
              ],
            )),
            Icon(Icons.chevron_right)
          ],
        ),
      ),
    );
  }
}
