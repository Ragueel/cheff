import 'package:cheff/models/appUserInfo.dart';
import 'package:cheff/models/packs.dart';
import 'package:cheff/pages/settings/locationsPage.dart';
import 'package:cheff/pages/settings/paymentInfoPage.dart';
import 'package:cheff/repository/userInfoProvider.dart';
import 'package:cheff/repository/userSubscriptionsProvider.dart';
import 'package:cheff/widgets/packs/packCard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SubscribePage extends StatefulWidget {
  final RestaurantPackRow packRow;

  const SubscribePage(this.packRow, {Key key}) : super(key: key);

  @override
  _SubscribePageState createState() => _SubscribePageState();
}

class _SubscribePageState extends State<SubscribePage> {
  Widget _getOrderInfo() {
    var prices = List<Widget>();

    for (int i = 0; i < widget.packRow.days.length; i++) {
      prices.add(PriceRow(widget.packRow.price / widget.packRow.days.length, widget.packRow.days[i].dayName));
    }
    return Column(
      children: prices,
    );
  }

  bool _isSubscribing = false;

  UserLocation location;
  UserCard card;

  List<Widget> _getLocation() {
    var prices = List<Widget>();
    prices.add(Text('SAMPLE Location'));
    return prices;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Subscribe'),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: SizedBox(
        width: 270,
        height: 52,
        child: FloatingActionButton.extended(
            onPressed: () {
              if (_isSubscribing) {
                return;
              }
              setState(() {
                _isSubscribing = true;
              });
              var uSubProvider = Provider.of<UserSubscriptionsProvider>(context, listen: false);
              uSubProvider.addSubscription(widget.packRow, location, card, onSubscribed);
            },
            label: Text(
              'Subscribe',
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w300),
            )),
      ),
      body: _isSubscribing
          ? Center(
              child: CircularProgressIndicator(),
            )
          : buildBody(),
    );
  }

  Container buildBody() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          SubscriptionBlock(
            'Order',
            _getOrderInfo(),
          ),
          Consumer<UserInfoProvider>(builder: (context, provider, child) {
            if (provider.userInfo.locations.isEmpty) {
              return SubscriptionBlock(
                  'Location',
                  SettingsButtonElement(
                    callback: () {
                      Navigator.push(context, MaterialPageRoute(builder: (cont) => LocationsPage()));
                    },
                    iconData: Icons.add_location_alt_outlined,
                    text: 'Choose location',
                  ));
            }
            location = provider.getPreferredLocation();
            return SubscriptionBlock('Location',
                LocationButton(location.streetName, 'Floor: ${location.floor}, Flat: ${location.flatNumber}'));
          }),
          Consumer<UserInfoProvider>(builder: (context, provider, child) {
            if (provider.userInfo.cards.isEmpty) {
              return SubscriptionBlock(
                  'CARD',
                  SettingsButtonElement(
                    callback: () {
                      Navigator.push(context, MaterialPageRoute(builder: (cont) => PaymentInfoPage()));
                    },
                    iconData: Icons.payment,
                    text: 'Choose payment Info',
                  ));
            }
            card = provider.getPreferredCard();
            return SubscriptionBlock('Card ending with', PaymentCardButton(card));
          }),
          SizedBox(
            height: 30,
          ),
          TotalPriceText(widget.packRow.price)
        ],
      ),
    );
  }

  void onSubscribed() {
    if (card == null || location == null) {
      return;
    }
    setState(() {
      _isSubscribing = false;
    });
    Navigator.pop(context);
  }
}

class SubscriptionBlock extends StatelessWidget {
  final Widget content;
  final String header;

  const SubscriptionBlock(this.header, this.content, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          LightHeaderWidget(header),
          Divider(),
          content,
        ],
      ),
    );
  }
}

class PriceRow extends StatelessWidget {
  final double price;
  final String dayName;

  const PriceRow(this.price, this.dayName, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 3),
      child: Row(
        children: [Expanded(child: Text(dayName)), Text(price.toString())],
      ),
    );
  }
}

class TotalPriceText extends StatelessWidget {
  final TextStyle _textStyle = TextStyle(fontSize: 20, fontWeight: FontWeight.w700);

  final double price;

  TotalPriceText(this.price, {key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                  child: Text(
                'Total price',
                style: _textStyle,
              )),
              Text(price.toString(), style: _textStyle)
            ],
          ),
          Divider()
        ],
      ),
    );
  }
}

class LightHeaderWidget extends StatelessWidget {
  final String text;

  const LightHeaderWidget(this.text, {key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          text.toUpperCase(),
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w300, color: Colors.grey[500]),
        )
      ],
    );
  }
}
