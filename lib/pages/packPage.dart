import 'package:cheff/models/packs.dart';
import 'package:cheff/pages/subscribePage.dart';
import 'package:cheff/repository/userSubscriptionsProvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:timelines/timelines.dart';

class PackPage extends StatelessWidget {
  PackPage({Key key, this.packRow}) : super(key: key);

  final RestaurantPackRow packRow;

  final TextStyle _textStyle = new TextStyle(fontWeight: FontWeight.bold, fontSize: 22);

  Widget _getContent() {
    return Container(
      child: Timeline.tileBuilder(
        theme: TimelineThemeData(color: Colors.red),
        builder: TimelineTileBuilder.fromStyle(
          nodePositionBuilder: (context, index) {
            return 0.05;
          },
          contentsAlign: ContentsAlign.basic,
          contentsBuilder: (context, index) => Padding(
            padding: const EdgeInsets.all(5.0),
            child: DayRow(packRow.days[index]),
          ),
          itemCount: packRow.days.length,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverPersistentHeader(
                floating: true, pinned: true, delegate: CustomSliverDelegate(packRow, expandedHeight: 130)),
            SliverFillRemaining(
              child: _getContent(),
            )
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Consumer<UserSubscriptionsProvider>(
        builder: (context, userSubscriptionProvider, child) {
          return Visibility(
              visible: !userSubscriptionProvider.hasSubscribedToPack(packRow.packId),
              child: SizedBox(
                width: 270,
                height: 52,
                child: FloatingActionButton.extended(
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (cont) => SubscribePage(packRow)));
                    },
                    label: Text(
                      'Subscribe',
                      style: TextStyle(fontSize: 22, fontWeight: FontWeight.w300),
                    )),
              ));
        },
      ),
    );
  }
}

class CustomSliverDelegate extends SliverPersistentHeaderDelegate {
  final double expandedHeight;
  final bool hideTitleWhenExpanded;
  final RestaurantPackRow packRow;

  CustomSliverDelegate(
    this.packRow, {
    @required this.expandedHeight,
    this.hideTitleWhenExpanded = true,
  });

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    final appBarSize = expandedHeight - shrinkOffset;
    final cardTopPosition = expandedHeight / 2 - shrinkOffset;
    final proportion = 2 - (expandedHeight / appBarSize);
    final percent = proportion < 0 || proportion > 1 ? 0.0 : proportion;
    return SizedBox(
      height: expandedHeight + expandedHeight / 2,
      child: Stack(
        children: [
          SizedBox(
            height: appBarSize < kToolbarHeight ? kToolbarHeight : appBarSize,
            child: AppBar(
              systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: Colors.red),
              iconTheme: IconThemeData(
                color: Colors.white, //change your color here
              ),
              backgroundColor: Theme.of(context).accentColor,
              leading: IconButton(
                icon: Icon(Icons.chevron_left),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              elevation: 0.0,
              title: Opacity(
                  opacity: hideTitleWhenExpanded ? 1.0 - percent : 1.0,
                  child: Text(
                    packRow.restaurantName,
                    style: TextStyle(color: Colors.white),
                  )),
            ),
          ),
          Positioned(
            left: 0.0,
            right: 0.0,
            top: cardTopPosition > 0 ? cardTopPosition : 0,
            bottom: 0.0,
            child: Opacity(
              opacity: percent,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 30 * percent),
                child: RestaurantCard(packRow),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  double get maxExtent => expandedHeight + expandedHeight / 2;

  @override
  double get minExtent => kToolbarHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}

class RestaurantCard extends StatelessWidget {
  final RestaurantPackRow packRow;

  const RestaurantCard(this.packRow, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 20.0,
      child: Center(
        child: Container(
          padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    packRow.restaurantName,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  )
                ],
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InfoIcon(
                    Icons.favorite,
                    packRow.rating.toString() + "%",
                    color: Colors.red,
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  InfoIcon(
                    Icons.attach_money_outlined,
                    packRow.price.toString(),
                    color: Colors.green,
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  InfoIcon(
                    Icons.delivery_dining,
                    packRow.deliveryTime,
                    color: Colors.blue,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

class InfoIcon extends StatelessWidget {
  final IconData iconData;
  final String text;
  final Color color;

  const InfoIcon(this.iconData, this.text, {Key key, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Icon(
          iconData,
          size: 40,
          color: color,
        ),
        Text(text)
      ],
    );
  }
}

class DayRow extends StatelessWidget {
  final TextStyle _textStyle = new TextStyle(fontWeight: FontWeight.bold, fontSize: 20);
  final DayInfo dayInfo;

  final double _paddingSize = 10;

  DayRow(this.dayInfo, {Key key}) : super(key: key);

  Widget _getContent() {
    return Container(
        child: Row(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(15),
          child: Image.network(
            dayInfo.imageUrl,
            width: 90,
            height: 100,
          ),
        ),
        Expanded(
          child: Column(
            children: [
              SizedBox(
                height: _paddingSize,
              ),
              Row(
                children: [
                  SizedBox(
                    width: _paddingSize,
                  ),
                  Expanded(
                    child: Text(
                      dayInfo.foodName,
                      style: _textStyle,
                    ),
                  )
                ],
              ),
              Expanded(
                child: Row(
                  children: [
                    SizedBox(
                      width: _paddingSize,
                    ),
                    Expanded(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          dayInfo.dayName,
                          style: TextStyle(fontWeight: FontWeight.w300),
                        ),
                        Text(
                          dayInfo.foodDescription,
                          overflow: TextOverflow.ellipsis,
                        )
                      ],
                    ))
                  ],
                ),
              ),
              SizedBox(
                height: _paddingSize,
              )
            ],
          ),
        )
      ],
    ));
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: _getContent(),
      onTap: () {},
    );
  }
}
