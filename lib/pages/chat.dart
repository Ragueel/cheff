import 'package:cheff/models/subscriptions.dart';
import 'package:cheff/pages/subscriptionOverviewPage.dart';
import 'package:cheff/repository/userSubscriptionsProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Chat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<UserSubscriptionsProvider>(
      builder: (context, provider, child) {
        if (provider.userSubscriptions.isEmpty) {
          return Container(
            child: Center(
              child: Text(
                'No subscriptions yet!',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w300),
              ),
            ),
          );
        }
        return Container(
            child: ListView.builder(
                itemCount: provider.userSubscriptions.length,
                itemBuilder: (context, index) {
                  return ChatRow(provider.userSubscriptions[index]);
                })
            // Column(children: [
            //   ChatSkeleton(),
            //   SizedBox(
            //     height: 10,
            //   ),
            //   ChatSkeleton(),
            //   SizedBox(
            //     height: 10,
            //   ),
            //   ChatSkeleton()
            // ])
            );
      },
    );
  }
}

class ChatRow extends StatelessWidget {
  final UserSubscription subscription;

  const ChatRow(this.subscription, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Row(
          children: [
            CircleAvatar(
              radius: 30,
              backgroundImage: NetworkImage(subscription.imageUrl),
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  subscription.restaurantName,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                Text('Delivery time: ${subscription.nextDeliveryDate}')
              ],
            )),
            SizedBox(
              width: 20,
            ),
            IconButton(icon: Icon(Icons.more_vert), onPressed: () {})
          ],
        ),
      ),
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (cont) => SubscriptionOverviewPage(subscription)));
      },
    );
  }
}
