import 'package:cheff/models/appUserInfo.dart';
import 'package:cheff/repository/userInfoProvider.dart';
import 'package:cheff/textFormatters/allUppercaseFormatter.dart';
import 'package:cheff/textFormatters/cardDateFormatter.dart';
import 'package:cheff/textFormatters/cardNumberFormatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class AddPaymentMethodPage extends StatefulWidget {
  const AddPaymentMethodPage({Key key}) : super(key: key);

  @override
  _AddPaymentMethodPageState createState() => _AddPaymentMethodPageState();
}

class _AddPaymentMethodPageState extends State<AddPaymentMethodPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController cardNumberController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController cvvController = TextEditingController();
  TextEditingController nameSurnameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add payment method'),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                        new LengthLimitingTextInputFormatter(19),
                        new CardNumberInputFormatter()
                      ],
                      controller: cardNumberController,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(labelText: 'Card Number', hintText: '1111 1111 1111 1111'),
                      validator: CardValidationUtils.notEmpty,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                      child: TextFormField(
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                      new LengthLimitingTextInputFormatter(4),
                      new CardDateFormatter()
                    ],
                    controller: dateController,
                    decoration: InputDecoration(labelText: 'Date', hintText: 'yy/mm'),
                    validator: CardValidationUtils.notEmpty,
                  )),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                      child: TextFormField(
                    keyboardType: TextInputType.number,
                    inputFormatters: [new LengthLimitingTextInputFormatter(3)],
                    obscureText: true,
                    controller: cvvController,
                    decoration: InputDecoration(labelText: 'CVV', hintText: '***'),
                    validator: CardValidationUtils.CVValidation,
                  ))
                ],
              ),
              Row(
                children: [
                  Expanded(
                      child: TextFormField(
                    inputFormatters: [new AllUppercaseFormatter()],
                    controller: nameSurnameController,
                    decoration: InputDecoration(labelText: 'NAME SURNAME', hintText: 'NAME SURNAME'),
                    validator: CardValidationUtils.notEmpty,
                  ))
                ],
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                children: [
                  Expanded(
                      child: RawMaterialButton(
                    fillColor: Theme.of(context).accentColor,
                    onPressed: _addCard,
                    child: Text(
                      'Add',
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                  ))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void _addCard() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    UserCard card =
        new UserCard(cardNumberController.text, dateController.text, cvvController.text, nameSurnameController.text);

    Provider.of<UserInfoProvider>(context, listen: false).addCard(card);
    Navigator.pop(context);
  }
}

class CardValidationUtils {
  static String notEmpty(String value) {
    if (value.isEmpty) {
      return 'Enter something';
    }
    return null;
  }

  static String CVValidation(String value) {
    var isEmpty = notEmpty(value);

    if (isEmpty != null) {
      return isEmpty;
    }

    if (value.length < 3) {
      return 'Should be 3 digits';
    }
    return null;
  }
}
