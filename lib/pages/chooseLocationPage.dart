import 'dart:developer';

import 'package:cheff/models/appUserInfo.dart';
import 'package:cheff/repository/userInfoProvider.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

var uuid = Uuid();

class ChooseLocationPage extends StatefulWidget {
  @override
  State<ChooseLocationPage> createState() => ChooseLocationPageState();
}

class ChooseLocationPageState extends State<ChooseLocationPage> {
  static final LatLng _kMapCenter = LatLng(43.2220, 76.8512);
  Marker marker;

  static final CameraPosition _kInitialPosition = CameraPosition(target: _kMapCenter, zoom: 11.0, tilt: 0, bearing: 0);
  Set<Marker> _markers = Set();
  GoogleMapController _controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Choose your location'),
        backgroundColor: Colors.white,
      ),
      body: buildGoogleMap(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: buildRawMaterialButton(),
    );
  }

  GoogleMap buildGoogleMap() {
    return GoogleMap(
      initialCameraPosition: _kInitialPosition,
      myLocationEnabled: true,
      myLocationButtonEnabled: true,
      onMapCreated: (map) async {
        _controller = map;
      },
      onTap: _handleTap,
      markers: _markers,
    );
  }

  Widget buildRawMaterialButton() {
    return FloatingActionButton.extended(
      onPressed: () {
        if (_markers.isEmpty) {
          return;
        }
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (cont) => ConfirmTheLocationInfoPage(marker)));
      },
      label: Text('Choose this location'),
    );
  }

  void _handleTap(LatLng argument) async {
    log(argument.toString());
    setState(() {
      var addresses = placemarkFromCoordinates(argument.latitude, argument.longitude);
      addresses.then((value) {
        marker = Marker(
            markerId: MarkerId("SelectedLocation"),
            position: argument,
            infoWindow: InfoWindow(title: value.first.street),
            rotation: 0);
        if (!_markers.contains(marker)) {
          _markers.add(marker);
        } else {
          _markers.clear();
          _markers.add(marker);
        }
      });
    });
  }
}

class ConfirmTheLocationInfoPage extends StatefulWidget {
  final Marker marker;

  const ConfirmTheLocationInfoPage(this.marker, {Key key}) : super(key: key);

  @override
  _ConfirmTheLocationInfoPageState createState() => _ConfirmTheLocationInfoPageState();
}

class _ConfirmTheLocationInfoPageState extends State<ConfirmTheLocationInfoPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController floorController = TextEditingController();
  TextEditingController flatNumberController = TextEditingController();
  TextEditingController additionalInfoController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add information'),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Text(
                widget.marker.infoWindow.title,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w800),
              ),
              TextFormField(
                  controller: floorController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(labelText: 'Floor', hintText: 'Floor'),
                  validator: (val) {
                    if (val.isEmpty) {
                      return 'Should not be empty';
                    }
                    return null;
                  }),
              TextFormField(
                controller: flatNumberController,
                decoration: InputDecoration(labelText: 'Flat number', hintText: 'Flat number'),
                validator: (val) {
                  if (val.isEmpty) {
                    return 'Should not be empty';
                  }
                  return null;
                },
              ),
              TextFormField(
                controller: additionalInfoController,
                decoration: InputDecoration(labelText: 'Additional info', hintText: 'Additional info'),
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                children: [
                  Expanded(
                      child: RawMaterialButton(
                    onPressed: () {
                      if (!_formKey.currentState.validate()) {
                        return;
                      }
                      UserLocation location = new UserLocation();
                      location.lat = widget.marker.position.latitude;
                      location.long = widget.marker.position.longitude;
                      location.additionalInfo = additionalInfoController.text;
                      location.flatNumber = flatNumberController.text;
                      location.floor = int.parse(floorController.text);
                      location.streetName = widget.marker.infoWindow.title;
                      var userProvider = Provider.of<UserInfoProvider>(context, listen: false);
                      userProvider.addLocation(location);
                      Navigator.pop(context);
                    },
                    fillColor: Theme.of(context).accentColor,
                    child: Text(
                      'Confirm',
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ),
                  ))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
