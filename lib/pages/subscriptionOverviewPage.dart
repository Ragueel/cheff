import 'package:cheff/models/subscriptions.dart';
import 'package:cheff/repository/userInfoProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SubscriptionOverviewPage extends StatefulWidget {
  final UserSubscription subscription;

  const SubscriptionOverviewPage(
    this.subscription, {
    Key key,
  }) : super(key: key);

  @override
  _SubscriptionOverviewPageState createState() => _SubscriptionOverviewPageState();
}

class _SubscriptionOverviewPageState extends State<SubscriptionOverviewPage> {
  @override
  Widget build(BuildContext context) {
    var userInfoProvider = Provider.of<UserInfoProvider>(context);
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            expandedHeight: 200,
            pinned: true,
            iconTheme: IconThemeData(
              color: Colors.white, //change your color here
            ),
            backgroundColor: Theme.of(context).accentColor,
            flexibleSpace: FlexibleSpaceBar(
              title: Text(
                widget.subscription.restaurantName,
                style: TextStyle(color: Colors.white),
              ),
              background: Image.network(
                widget.subscription.imageUrl,
                fit: BoxFit.cover,
              ),
            ),
          ),
          SliverFillRemaining(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  SizedBox(
                    height: 30,
                  ),
                  Text(
                    'Delivery date: ${widget.subscription.nextDeliveryDate}',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  SubscriptionInfoBlock(Icons.card_membership,
                      'Card ending with: ****${userInfoProvider.userInfo.cards[0].cardNumber.substring(userInfoProvider.userInfo.cards[0].cardNumber.length - 4, userInfoProvider.userInfo.cards[0].cardNumber.length)}'),
                  SubscriptionInfoBlock(Icons.location_on_outlined, userInfoProvider.userInfo.locations[0].streetName),
                  SubscriptionInfoBlock(
                      Icons.system_update_alt_outlined, "Current status: ${widget.subscription.currentStatus}"),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class SubscriptionInfoBlock extends StatelessWidget {
  final IconData iconData;
  final String text;

  const SubscriptionInfoBlock(this.iconData, this.text, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      child: Row(
        children: [
          Icon(
            iconData,
            size: 30,
          ),
          SizedBox(
            width: 20,
          ),
          Expanded(
              child: Text(
            text,
            style: TextStyle(fontSize: 17),
          ))
        ],
      ),
    );
  }
}
