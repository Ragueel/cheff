import 'package:cheff/pages/settings/faqPage.dart';
import 'package:cheff/pages/settings/locationsPage.dart';
import 'package:cheff/pages/settings/paymentInfoPage.dart';
import 'package:cheff/pages/settings/personalInfoPage.dart';
import 'package:cheff/repository/userInfoProvider.dart';
import 'package:cheff/widgets/packs/packCard.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Settings extends StatelessWidget {
  Widget _getProfile() {
    return Row(
      children: [
        SizedBox(
          width: 16,
        ),
        Consumer<UserInfoProvider>(builder: (context, provider, childer) {
          return Text(
            'Welcome, ${provider.userInfo.name}',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
            textAlign: TextAlign.center,
          );
        })
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
            alignment: Alignment.center,
            child: Column(
              children: [
                SizedBox(
                  height: 32,
                ),
                _getProfile(),
                SizedBox(
                  height: 16,
                ),
                SettingsButtonElement(
                  callback: () {
                    Navigator.push(context, MaterialPageRoute(builder: (cont) => PersonalInfoPage()));
                  },
                  iconData: Icons.verified_user_outlined,
                  text: 'Personal info',
                ),
                Divider(),
                SettingsButtonElement(
                  callback: () {
                    Navigator.push(context, MaterialPageRoute(builder: (cont) => PaymentInfoPage()));
                  },
                  iconData: Icons.payment,
                  text: 'Payment Info',
                ),
                SettingsButtonElement(
                  callback: () {
                    Navigator.push(context, MaterialPageRoute(builder: (cont) => LocationsPage()));
                  },
                  iconData: Icons.add_location_alt_outlined,
                  text: 'Locations',
                ),
                SettingsButtonElement(
                  callback: () {
                    Navigator.push(context, MaterialPageRoute(builder: (cont) => FaqPage()));
                  },
                  iconData: Icons.help_outline,
                  text: 'FAQ',
                ),
                SettingsButtonElement(
                  callback: () {
                    FirebaseAuth.instance.signOut();
                  },
                  iconData: Icons.logout,
                  text: 'Logout',
                )
              ],
            )));
  }
}
