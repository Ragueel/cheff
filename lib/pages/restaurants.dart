import 'dart:math';

import 'package:cheff/models/packs.dart';
import 'package:cheff/repository/packsProvider.dart';
import 'package:cheff/widgets/packs/packCard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Packs extends StatefulWidget {
  @override
  _PacksState createState() => _PacksState();
}

class _PacksState extends State<Packs> {
  List<RestaurantPackRow> _restaurants = <RestaurantPackRow>[];
  TextEditingController searchController = TextEditingController();

  void _onAllClick() {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(35.0), topRight: Radius.circular(35.0)),
        ),
        builder: (context) {
          return AlertCategoriesGrid();
        });
  }

  Widget _getCategories() {
    return Row(
      children: [
        Expanded(
            child: CategoryButton(
          text: 'All',
          icon: Icons.more_horiz,
          onClick: _onAllClick,
        )),
        Expanded(child: CategoryButton(text: 'Healthy', icon: Icons.park)),
        Expanded(child: CategoryButton(text: 'Salads', icon: Icons.food_bank_outlined)),
        Expanded(child: CategoryButton(text: 'Coffee', icon: Icons.emoji_food_beverage_rounded)),
      ],
    );
  }

  Widget _getFlexibleSpaceBarBackground() {
    return Column(
      children: <Widget>[
        SizedBox(height: 85.0),
        Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 6.0, 10.0, 10.0),
          child: Container(
            height: 38.0,
            width: double.infinity,
            child: CupertinoTextField(
              keyboardType: TextInputType.text,
              controller: searchController,
              placeholder: 'Search',
              placeholderStyle: TextStyle(
                color: Color(0xffC4C6CC),
                fontSize: 17.0,
                fontFamily: 'Brutal',
              ),
              prefix: Padding(
                padding: const EdgeInsets.fromLTRB(9.0, 6.0, 9.0, 6.0),
                child: Icon(
                  Icons.search,
                  color: Color(0xffC4C6CC),
                ),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(90.0),
                color: Color(0xffF0F1F5),
              ),
            ),
          ),
        ),
        SizedBox(height: 10.0),
        _getCategories()
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomScrollView(
      slivers: [
        SliverAppBar(
          expandedHeight: 210,
          pinned: true,
          title: Text('Packs'),
          backgroundColor: Colors.white,
          flexibleSpace: FlexibleSpaceBar(background: _getFlexibleSpaceBarBackground()),
          actions: [Icon(Icons.event)],
        ),
        Consumer<PacksProvider>(
          builder: (context, provider, child) {
            provider.loadData();

            return SliverList(
                delegate: SliverChildBuilderDelegate((context, index) {
              return PackCard(packRow: provider.packs[index]);
            }, childCount: provider.packs.length));
          },
        )
      ],
    ));
  }
}

const String _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';

Random _rnd = Random();

class CategoryButton extends StatelessWidget {
  final String text;
  final VoidCallback onClick;
  final IconData icon;

  const CategoryButton({
    key,
    this.text,
    this.icon,
    this.onClick,
  }) : super(key: key);

  String getRandomString(int length) =>
      String.fromCharCodes(Iterable.generate(length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FloatingActionButton(
          heroTag: getRandomString(15),
          onPressed: () {
            onClick();
          },
          backgroundColor: Colors.red[200],
          elevation: 0,
          child: Icon(icon),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          text,
          style: TextStyle(fontSize: 12.0),
        )
      ],
    );
  }
}

class AlertCategoriesGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Text(
            'Choose category',
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 22),
          ),
          SizedBox(
            height: 5,
          ),
          Divider(),
          SizedBox(
            height: 5,
          ),
          Expanded(
              child: GridView.count(
            padding: EdgeInsets.all(10.0),
            crossAxisSpacing: 10,
            mainAxisSpacing: 5,
            crossAxisCount: 4,
            children: <Widget>[
              CategoryButton(text: 'Coffee', icon: Icons.emoji_food_beverage_rounded),
              CategoryButton(text: 'Coffee', icon: Icons.emoji_food_beverage_rounded),
              CategoryButton(text: 'Coffee', icon: Icons.emoji_food_beverage_rounded),
              CategoryButton(text: 'Coffee', icon: Icons.emoji_food_beverage_rounded),
              CategoryButton(text: 'Coffee', icon: Icons.emoji_food_beverage_rounded),
              CategoryButton(text: 'Coffee', icon: Icons.emoji_food_beverage_rounded),
              CategoryButton(text: 'Coffee', icon: Icons.emoji_food_beverage_rounded),
              CategoryButton(text: 'Coffee', icon: Icons.emoji_food_beverage_rounded),
              CategoryButton(text: 'Coffee', icon: Icons.emoji_food_beverage_rounded),
              CategoryButton(text: 'Coffee', icon: Icons.emoji_food_beverage_rounded),
              CategoryButton(text: 'Coffee', icon: Icons.emoji_food_beverage_rounded),
              CategoryButton(text: 'Coffee', icon: Icons.emoji_food_beverage_rounded),
            ],
          ))
        ],
      ),
    );
  }
}

class CategoryUtils {
  static IconData getIconForCategory(String categoryName) {
    if (categoryName == "coffee") {
      return Icons.emoji_food_beverage_rounded;
    }

    if (categoryName == "fast_food") {
      return Icons.fastfood_outlined;
    }

    return Icons.emoji_food_beverage_rounded;
  }
}
