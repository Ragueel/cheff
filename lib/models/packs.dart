class RestaurantPackRow {
  String packId;
  String restaurantName;
  String imageUrl;
  double price;
  double rating;
  String deliveryTime;
  List<String> categories = [];
  List<DayInfo> days = [];

  RestaurantPackRow(this.packId, this.restaurantName);
}

class WeekInfo {
  String weekName;
  List<DayInfo> days;
}

class DayInfo {
  String dayName;
  String foodName;
  String foodDescription;
  String imageUrl;
  double regularPrice;
}
