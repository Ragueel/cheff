class UserSubscription {
  String id;
  String packId;
  String currentStatus;
  String nextDeliveryDate;
  String restaurantName;
  String imageUrl;
  String cardId;
  String locationId;
}
