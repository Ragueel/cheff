class AppUserInfo {
  String name;
  List<UserCard> cards = [];
  List<UserLocation> locations = [];
}

class UserLocation {
  String id;
  double lat;
  double long;
  String streetName;
  int floor;
  String flatNumber;
  String additionalInfo;
}

class UserCard {
  String cardId;
  String cardNumber;
  String date;
  String CVV;
  String cardName;

  UserCard(this.cardNumber, this.date, this.CVV, this.cardName);
}
