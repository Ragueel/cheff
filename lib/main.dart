import 'dart:developer' as developer;
import 'dart:io';

import 'package:cheff/repository/packsProvider.dart';
import 'package:cheff/repository/userInfoProvider.dart';
import 'package:cheff/repository/userSubscriptionsProvider.dart';
import 'package:cheff/startUp/authentication.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'pages/chat.dart';
import 'pages/restaurants.dart';
import 'pages/settings.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Future<FirebaseApp> _initialization;
  bool _userIsSignedIn = false;

  @override
  void initState() {
    _initialization = Firebase.initializeApp();
    _initialization.then((value) => {
          FirebaseAuth.instance.authStateChanges().listen((User user) {
            if (user == null) {
              print('User signed out');
              setState(() {
                _userIsSignedIn = false;
              });
            } else {
              print('User signed in');
              setState(() {
                _userIsSignedIn = true;
              });
            }
          })
        });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: Colors.redAccent));
    if (_initialization == null) {
      _initialization = Firebase.initializeApp();
    }
    return FutureBuilder(
      future: _initialization,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          stdout.addError('Error could not load firebase' + snapshot.error.toString());
        }

        if (snapshot.connectionState == ConnectionState.done) {
          developer.log('Connected to firebase');
        }

        // var user = FirebaseAuth.instance.currentUser;

        return MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (_) => PacksProvider()),
            ChangeNotifierProvider(create: (_) => UserInfoProvider()),
            ChangeNotifierProvider(create: (_) => UserSubscriptionsProvider())
          ],
          child: MaterialApp(
            title: 'Cheff',
            theme: ThemeData(primaryColor: Colors.red[200], accentColor: Colors.redAccent),
            home: Scaffold(
              body: _userIsSignedIn ? Center(child: MainWidget()) : AuthenticationPage(),
            ),
          ),
        );
      },
    );
  }
}

class MainWidget extends StatefulWidget {
  @override
  _MainWidgetState createState() => _MainWidgetState();
}

class _MainWidgetState extends State<MainWidget> {
  int _selectedIndex = 0;
  List<Widget> _widgetOptions = <Widget>[Packs(), Chat(), Settings()];

  void _pushSaved() {
    Navigator.of(context).push(MaterialPageRoute<void>(builder: (BuildContext context) {
      return Scaffold(
        appBar: AppBar(title: Text('Sample')),
      );
    }));
  }

  @override
  void initState() {
    super.initState();
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    User user = FirebaseAuth.instance.currentUser;
    var userProvider = Provider.of<UserInfoProvider>(context);
    var subsProvider = Provider.of<UserSubscriptionsProvider>(context);

    subsProvider.loadData();
    userProvider.loadData();
    return Scaffold(
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.bookmark_border_rounded), label: 'Restaurants'),
          BottomNavigationBarItem(icon: Icon(Icons.chat_bubble_outline), label: 'Subscriptions'),
          BottomNavigationBarItem(icon: Icon(Icons.settings_outlined), label: 'Settings')
        ],
        selectedItemColor: Colors.redAccent,
        unselectedItemColor: Colors.grey,
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}
