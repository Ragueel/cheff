import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class PlaceHolderImage extends StatefulWidget {
  const PlaceHolderImage({key, this.url = 'https://placeimg.com/450/220/any'})
      : super(key: key);

  final String url;

  @override
  _PlaceHolderImageState createState() => _PlaceHolderImageState();
}

class _PlaceHolderImageState extends State<PlaceHolderImage> {
  final _greyColor = Color.fromRGBO(220, 220, 220, 1);

  Widget _getLoadingImageShimmer() {
    return Shimmer.fromColors(
        child: Container(
          height: 180,
          color: Colors.redAccent,
        ),
        baseColor: _greyColor,
        highlightColor: Colors.white10);
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10.0),
      child: Image.network(
        widget.url,
        loadingBuilder: (context, child, imageChunkEvent) {
          if (imageChunkEvent == null) {
            return child;
          }
          return _getLoadingImageShimmer();
        },
      ) ,
    );
  }
}
