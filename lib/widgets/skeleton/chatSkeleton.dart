import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ChatSkeleton extends StatelessWidget {
  final _greyColor = Color.fromRGBO(220, 220, 220, 1);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        child: Row(children: [
          CircleAvatar(
            radius: 15,
            backgroundColor: _greyColor,
          ),
          SizedBox(
            width: 15,
          ),
          Expanded(
            child: Container(
              height: 30,
              child: Shimmer.fromColors(
                  baseColor: _greyColor,
                  highlightColor: Colors.white,
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    // height: 20,
                    color: Colors.red,
                    // padding: EdgeInsets.symmetric(vertical: 5)))
                  )),
            ),
          ),
        ]));
  }
}
