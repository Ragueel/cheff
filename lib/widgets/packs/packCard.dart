import 'package:cheff/models/packs.dart';
import 'package:cheff/pages/packPage.dart';
import 'package:cheff/widgets/imageWithPlaceHolder.dart';
import 'package:flutter/material.dart';

class PackCard extends StatelessWidget {
  static const imageTextStyle = TextStyle(color: Colors.white, fontSize: 25.0, fontWeight: FontWeight.bold);

  const PackCard({Key key, this.packRow}) : super(key: key);

  final RestaurantPackRow packRow;

  Widget _getRatingWidget(RestaurantPackRow orderRow) {
    return Row(
      children: [
        SizedBox(
          width: 15,
        ),
        Icon(Icons.favorite, color: Colors.redAccent),
        SizedBox(
          width: 5,
        ),
        Text(
          orderRow.rating.toString() + "%",
          style: TextStyle(fontWeight: FontWeight.w600, fontSize: 15.0),
        )
      ],
    );
  }

  List<Widget> _getCategories() {
    List<Widget> categories = [];
    for (int i = 0; i < packRow.categories.length; i++) {
      categories.add(Container(
        child: Chip(
          backgroundColor: Colors.black54,
          label: Text(
            packRow.categories[i],
            style: TextStyle(color: Colors.white),
          ),
        ),
      ));
      categories.add(SizedBox(
        width: 5.0,
      ));
    }
    return categories;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PackPage(
                      packRow: packRow,
                    )));
      },
      child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          child: Column(
            children: [
              Stack(
                alignment: Alignment.center,
                children: [
                  PlaceHolderImage(url: packRow.imageUrl),
                  Center(
                      child: Column(
                    children: [
                      Text(
                        packRow.restaurantName != null ? packRow.restaurantName : 'Restaurant',
                        style: imageTextStyle,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: _getCategories(),
                      ),
                    ],
                  ))
                ],
              ),
              SizedBox(
                height: 5,
              ),
              Row(children: [
                Expanded(
                  child: _getRatingWidget(packRow),
                ),
                Row(
                  children: [
                    Icon(
                      Icons.date_range,
                      color: Colors.black54,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      '1 Week',
                      textAlign: TextAlign.end,
                      style: TextStyle(fontWeight: FontWeight.w800),
                    )
                  ],
                ),
                SizedBox(
                  width: 25,
                ),
              ]),
              SizedBox(
                height: 8,
              ),
            ],
          )),
    );
  }
}

class SettingsButtonElement extends StatelessWidget {
  final VoidCallback callback;
  final String text;
  final IconData iconData;

  const SettingsButtonElement({Key key, this.callback, this.text, this.iconData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: FlatButton(
            onPressed: callback,
            padding: EdgeInsets.all(16),
            child: Row(
              children: [
                Icon(iconData),
                SizedBox(
                  width: 16,
                ),
                Expanded(child: Text(text)),
                Icon(Icons.chevron_right)
              ],
            ),
          ),
        ),
      ],
    );
  }
}
